//
//  ViewController.m
//  TestCoreLocation
//
//  Created by Avi Cohen on 11/4/14.
//  Copyright (c) 2014 Avi Cohen. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

#define SLog(fmt, ...) NSLog((@"success: %s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define FLog(fmt, ...) NSLog((@"fail: %s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

@interface ViewController () <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *altitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UILabel *regionLabel;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSNumberFormatter *formatter;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.formatter = [NSNumberFormatter new];
    self.formatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    BOOL iOS8OrLater = ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0);
    if(iOS8OrLater) {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    FLog(@"%@", error.localizedDescription);
    [self.locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *crnLoc = [locations lastObject];
    SLog(@"%@", crnLoc);
    self.latitudeLabel.text = [self.formatter stringFromNumber:@(crnLoc.coordinate.latitude)];
    self.longitudeLabel.text = [self.formatter stringFromNumber:@(crnLoc.coordinate.longitude)];
    self.altitudeLabel.text = [self.formatter stringFromNumber:@(crnLoc.altitude)];
    self.speedLabel.text = [self.formatter stringFromNumber: @(crnLoc.speed)];
    [self.locationManager stopUpdatingLocation];
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:crnLoc.coordinate radius:50 identifier:@"initialLocation"];
    region.notifyOnEntry = YES;
    region.notifyOnExit = YES;
    [self.locationManager startMonitoringForRegion:region];
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    FLog(@"%@", error.localizedDescription);
    self.regionLabel.text = NSLocalizedString(@"error", nil);
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    SLog(@"%@", region);
    self.regionLabel.text = NSLocalizedString(@"Outside", nil);
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    SLog(@"%@", region);
    self.regionLabel.text = NSLocalizedString(@"Inside", nil);
}


@end
